package model.battleField;

import java.util.ArrayList;

import model.army.Army;
import model.army.unitHasMunition.Archer;
import model.army.unitHasMunition.Bomber;
import model.army.unitHasMunition.Catapult;
import model.army.unitHasMunition.Gun;
import model.army.unitHasMunition.Mortar;
import model.army.unitHasMunition.Slinger;
import model.army.unitHasNotMunition.Cook;
import model.army.unitHasNotMunition.Doctor;
import model.army.unitHasNotMunition.Hoplite;
import model.army.unitHasNotMunition.Ram;
import model.army.unitHasNotMunition.Spearman;
import model.army.unitHasNotMunition.SteamGiant;
import model.army.unitHasNotMunition.Swordsman;

public class Reserve {
  
  public Reserve() {
    armies = new ArrayList<>();
  }
  
  private ArrayList<Army> armies;
  
  public Army getAllArmy() {
    Army allArmy = new Army();
    
    for (Army army : armies) {
      
      for (Archer archer: army.getArchers()) {
        if (archer.getHitPoint() > 0) {
          allArmy.getArchers().add(archer);
        }
      }
      
      for (Bomber bomber: army.getBombers()) {
        if (bomber.getHitPoint() > 0) {
          allArmy.getBombers().add(bomber);
        }
      }
      
      for (Catapult catapult: army.getCatapults()) {
        if (catapult.getHitPoint() > 0) {
          allArmy.getCatapults().add(catapult);
        }
      }
      
      for (Cook cook: army.getCooks()) {
        if (cook.getHitPoint() > 0) {
          allArmy.getCooks().add(cook);
        }
      }
      
      for (Doctor doctor: army.getDoctors()) {
        if (doctor.getHitPoint() > 0) {
          allArmy.getDoctors().add(doctor);
        }
      }
      
      for (Gun gun: army.getGuns()) {
        if (gun.getHitPoint() > 0) {
          allArmy.getGuns().add(gun);
        }
      }
      
      for (Hoplite hoplite: army.getHoplites()) {
        if (hoplite.getHitPoint() > 0) {
          allArmy.getHoplites().add(hoplite);
        }
      }
      
      for (Mortar mortar: army.getMortars()) {
        if (mortar.getHitPoint() > 0) {
          allArmy.getMortars().add(mortar);
        }
      }
      
      for (Ram ram: army.getRams()) {
        if (ram.getHitPoint() > 0) {
          allArmy.getRams().add(ram);
        }
      }
      
      for (Slinger slinger: army.getSlingers()) {
        if (slinger.getHitPoint() > 0) {
          allArmy.getSlingers().add(slinger);
        }
      }
      
      for (Spearman spearman: army.getSpearmans()) {
        if (spearman.getHitPoint() > 0) {
          allArmy.getSpearmans().add(spearman);
        }
      }
      for (SteamGiant steamGiant: army.getSteamGiants()) {
        if (steamGiant.getHitPoint() > 0) {
          allArmy.getSteamGiants().add(steamGiant);
        }
      }
      
      for (Swordsman swordsman: army.getSwordsmans()) {
        if (swordsman.getHitPoint() > 0) {
          allArmy.getSwordsmans().add(swordsman);
        }
      }
    }

    return allArmy;
    
  }

  public ArrayList<Army> getArmies() {
    return armies;
  }
  
  
}
