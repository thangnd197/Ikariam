package model.battleField;

import model.army.Army;
import model.army.Wall;
import model.battleField.line.AirDefenseLine;
import model.battleField.line.ArtilleyLine;
import model.battleField.line.BomberLine;
import model.battleField.line.FlankLine;
import model.battleField.line.FrontLine;
import model.battleField.line.LeftFlankLine;
import model.battleField.line.LongRangeLine;
import model.battleField.line.RightFlankLine;

  /**
   * 
   * @author Thangkt
   *
   */
  public class BattleField {
    
    protected Army army;
    
    protected FrontLine front;
     
    protected LongRangeLine longRange;
    
    protected ArtilleyLine artilley;
    
    protected RightFlankLine rightFlank;
    
    protected LeftFlankLine leftFlank;
    
    protected BomberLine bomber;
    
    protected AirDefenseLine airDefense;
  
  /**
   * Constructor
   * 
   * @param levelOfTown
   * @param army
   */
  public BattleField(int levelOfTown, Army army) {
    
    this.army = army;
    
    setLine(levelOfTown);

  }

  /**
   * trigger for attack of each unit
   * 
   * @param enermyBattle
   */
  public void processRound(BattleField enermyBattle) {
    
    /**
     * Front Line attack front line
     * Loop with Cells
     */
    for (int i = 0; i < front.getCells().length; i++) {
      
      if (front.getCells()[i] != null && enermyBattle.getFront().getCells()[i] != null) {
        int k = enermyBattle.getFront().getCells()[i].getUnits().size();
        int p = 0;
                
        // Loop with Units
        for (int j = 0; j < front.getCells()[i].getUnits().size(); j++) {
          if (p == k) {
            p = 0;
          }
          front.getCells()[i].getUnits().get(j).attack(
              enermyBattle.getFront().getCells()[i].getUnits().get(p)
              );
          p++;
          
        }
      }
      
    }

    /**
     * Long Rang attack front line
     * Loop with Cells 
     */
    for (int i = 0; i < longRange.getCells().length; i++) {
      
      if (longRange.getCells()[i] != null && enermyBattle.getFront().getCells()[i] != null
          && ! (enermyBattle.getFront().getCells()[i].getUnits().get(0) instanceof Wall) ) {
        
        int k = enermyBattle.getFront().getCells()[i].getUnits().size();
        int p = 0;
                
        // Loop with Units
        for (int j = 0; j < longRange.getCells()[i].getUnits().size(); j++) {
          if (p == k) {
            p = 0;
          }
          longRange.getCells()[i].getUnits().get(j).attack(
              enermyBattle.getFront().getCells()[i].getUnits().get(p)
              );
          p++;
          
        }
      }
    }
    
    /*
     * Artilley attack front line
     * Loop with Cells
     */
    for (int i = 0; i < artilley.getCells().length; i++) {
      
      if (artilley.getCells()[i] != null && enermyBattle.getFront().getCells()[i] != null) {
        int k = enermyBattle.getFront().getCells()[i].getUnits().size();
        int p = 0;
                
        // Loop with Units
        for (int j = 0; j < artilley.getCells()[i].getUnits().size(); j++) {
          if (p == k) {
            p = 0;
          }
          artilley.getCells()[i].getUnits().get(j).attack(
              enermyBattle.getFront().getCells()[i].getUnits().get(p)
              );
          p++;
          
        }
      }
      
    }
    
  }

  /**
   * Set size of lines by level of Town
   * 
   * @param levelOfTown
   */
  private void setLine(int levelOfTown) {
    
    if (levelOfTown < 5) {
      
      front = new FrontLine(3, 30, 7, army);
      
      longRange = new LongRangeLine(3, 30, 7, army);
      
      artilley = new ArtilleyLine(1, 30, 7, army);
      
      FlankLine flank = new FlankLine(0, 0, 6, army);
      
      rightFlank = new RightFlankLine(0, 0, 3, flank);
      
      leftFlank = new LeftFlankLine(0, 0, 3, flank);
      
      bomber = new BomberLine(1, 10, 2, army);
      
      airDefense = new AirDefenseLine(1, 10, 2, army);
      
    }else if (levelOfTown < 10) {
      
      front = new FrontLine(5, 30, 7, army);
      
      longRange = new LongRangeLine(5, 30, 7, army);
      
      artilley = new ArtilleyLine(2, 30, 7, army);
      
      FlankLine flank = new FlankLine(2, 30, 6, army);
      
      rightFlank = new RightFlankLine(1, 30, 3, flank);
      
      leftFlank = new LeftFlankLine(1, 30, 3, flank);
      
      bomber = new BomberLine(1, 20, 2, army);
      
      airDefense = new AirDefenseLine(1, 20, 2, army);

    }else if (levelOfTown < 17) {
      
      front = new FrontLine(7, 30, 7, army);
      
      longRange = new LongRangeLine(7, 30, 7, army);
      
      artilley = new ArtilleyLine(3, 30, 7, army);
      
      FlankLine flank = new FlankLine(4, 30, 6, army);
      
      rightFlank = new RightFlankLine(2, 30, 3, flank);
      
      leftFlank = new LeftFlankLine(2, 30, 3, flank);
      
      bomber = new BomberLine(1, 30, 2, army);
      
      airDefense = new AirDefenseLine(1, 30, 2, army);

    }else if (levelOfTown < 25) {
      
      front = new FrontLine(7, 40, 7, army);
      
      longRange = new LongRangeLine(7, 40, 7, army);
      
      artilley = new ArtilleyLine(4, 30, 7, army);
      
      FlankLine flank = new FlankLine(6, 30, 6, army);
      
      rightFlank = new RightFlankLine(3, 30, 3, flank);
      
      leftFlank = new LeftFlankLine(3, 30, 3, flank);
      
      bomber = new BomberLine(2, 20, 2, army);
      
      airDefense = new AirDefenseLine(2, 20, 2, army);

    }else {
      
      front = new FrontLine(7, 50, 7, army);
      
      longRange = new LongRangeLine(7, 50, 7, army);
      
      artilley = new ArtilleyLine(5, 30, 7, army);
      
      FlankLine flank = new FlankLine(6, 40, 6, army);
      
      rightFlank = new RightFlankLine(3, 40, 3, flank);
      
      leftFlank = new LeftFlankLine(3, 40, 3, flank);
      
      bomber = new BomberLine(2, 30, 2, army);
      
      airDefense = new AirDefenseLine(2, 30, 2, army);

    }     
  }


  // ============================== Get Set field ================================================================
  public Army getArmy() {
    return army;
  }



  public FrontLine getFront() {
    return front;
  }



  public LongRangeLine getLongRange() {
    return longRange;
  }



  public ArtilleyLine getArtilley() {
    return artilley;
  }



  public RightFlankLine getRightFlank() {
    return rightFlank;
  }



  public LeftFlankLine getLeftFlank() {
    return leftFlank;
  }



  public BomberLine getBomber() {
    return bomber;
  }



  public AirDefenseLine getAirDefense() {
    return airDefense;
  }
  
  
}






















