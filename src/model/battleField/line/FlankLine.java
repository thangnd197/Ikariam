package model.battleField.line;

import model.army.Army;
import model.army.inforOfUnit;
import model.battleField.Cell;

public class FlankLine extends Line {

  public FlankLine(int theNumberOfCell, int sizeOfCell, int sizeOfArray, Army army) {
    super(theNumberOfCell, sizeOfCell, sizeOfArray, army);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void sort(Army army) {
    
    int n = theNumberOfCell;
    
    int j = 7 / 2;

    for (int i = 0; i < n; i++) {
      
      if (i % 2 == 0) {
        i = - i;
      }
      
      j = j - i;
      // only change bellow
      
      // swordsman full of cell
      if (army.getSwordsmans().size()
          / (sizeOfCell / inforOfUnit.SWORDSMAN[2]) > 0) {
        
        cells[j] = new Cell();
        
        for (int x = 0; x < sizeOfCell / inforOfUnit.SWORDSMAN[2]; x++) {
          
          cells[j].getUnits().add(army.getSwordsmans().remove(0));
          
        }
        
        // swordsman and spearman not full of cell ==> add swordsman
      } else if (! army.getSwordsmans().isEmpty() 
          && army.getSpearmans().size() 
            / (sizeOfCell / inforOfUnit.SPEARMAN[2]) < 1) {
        
        cells[j] = new Cell();
        
        for (int x = 0; x < army.getSwordsmans().size(); x++) {
          
          cells[j].getUnits().add(army.getSwordsmans().remove(0));
          
        }
        
        // swordsman not full of cell and spearman full of cell
      } else if (army.getSpearmans().size() 
          / (sizeOfCell / inforOfUnit.SPEARMAN[2]) > 0) {
        
        cells[j] = new Cell();
        
        for (int x = 0; x < sizeOfCell / inforOfUnit.SPEARMAN[2]; x++) {
          
          cells[j].getUnits().add(army.getSpearmans().remove(0));
          
        }
        
        // swordsman empty and spearman not full of cell
      } else if (! army.getSpearmans().isEmpty()) {
        
        cells[j] = new Cell();
        
        for (int x = 0; x < army.getSpearmans().size(); x++) {
          
          cells[j].getUnits().add(army.getSpearmans().remove(0));
          
        }
        
      }
      // only change above
      i = Math.abs(i);
    }
    
  }

}
