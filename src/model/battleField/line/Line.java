package model.battleField.line;

import model.army.Army;
import model.battleField.Cell;

public abstract class Line {
  
  public Line(int theNumberOfCell, int sizeOfCell, int sizeOfArray, Army army) {
    this.theNumberOfCell = theNumberOfCell;
    
    this.sizeOfCell = sizeOfCell;
    
    this.cells = new Cell[sizeOfArray];
    
    sort (army);
    
  }
  
  public abstract void sort(Army army);

  protected int sizeOfCell;
  
  protected int theNumberOfCell;
  
  protected Cell[] cells;
  
  public Cell[] getCells() {
    return cells;
  }

  
  
}
