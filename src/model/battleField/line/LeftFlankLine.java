package model.battleField.line;

import model.battleField.Cell;

public class LeftFlankLine {

  public LeftFlankLine(int theNumberOfCell, int sizeOfCell, 
      int sizeOfArray, FlankLine flankLine) {
    
    this.theNumberOfCell = theNumberOfCell;
    
    this.sizeOfCell = sizeOfCell;
    
    this.cells = new Cell[sizeOfArray];
    
    sort (flankLine);
    
  }
  
  public void sort(FlankLine flankLine) {
    for (int i = 2; i >= 0 ; i --) {
      cells[i] = flankLine.getCells()[i];
    }
  }

  protected int sizeOfCell;
  
  protected int theNumberOfCell;
  
  protected Cell[] cells;
  
  public Cell[] getCells() {
    return cells;
  }

}
