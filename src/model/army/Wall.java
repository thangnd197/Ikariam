package model.army;

public class Wall extends Unit{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  /**
   * Constructor
   * 
   * @param hitPoint
   * @param armour
   * @param size
   * @param rankArmour
   * @param rankDamage
   * @param accuracy
   * @param speed
   * @param damage
   */
  public Wall(int hitPoint, int armour, int size, int rankArmour, 
      int rankDamage, int accuracy, int speed, int damage) {
    super(hitPoint, armour, size, rankArmour, rankDamage, accuracy, speed, damage);

  }

  @Override
  public void attack(Unit unit) {
    
    if (rand.nextInt(100) + 1 > accuracy) {
      return;
    }
    
    if (unit.getArmour() - damage < 0) {
      unit.setHitPoint(unit.getHitPoint() + unit.getArmour() - damage);
    }
  }

  @Override
  public String getImg() {
    
    return "/img/wall.gif";
  }

}
