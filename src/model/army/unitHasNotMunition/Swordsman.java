package model.army.unitHasNotMunition;

import model.army.Unit;

public class Swordsman extends UnitHasNotMunition {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public Swordsman(int hitPoint, int armour, int size, int rankArmour, 
      int rankDamage, int accuracy, int speed,
      int damage) {
    super(hitPoint, armour, size, rankArmour, rankDamage, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }

  @Override
  public void attack(Unit unit) {
    
    if (rand.nextInt(100) + 1 > accuracy) {
      return;
    }
    
    if (unit.getArmour() - damage < 0) {
      unit.setHitPoint(unit.getHitPoint() + unit.getArmour() - damage);
    }    
  }

  @Override
  public String getImg() {
    // TODO Auto-generated method stub
    return "/img/Swordsman.gif";
  }

}
