package model.army.unitHasNotMunition;

import model.army.Unit;

public abstract class UnitHasNotMunition extends Unit{

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public UnitHasNotMunition(int hitPoint, int armour, int size, 
      int rankArmour, int rankDamage, int accuracy, int speed,
      int damage) {
    super(hitPoint, armour, size, rankArmour, rankDamage, accuracy, speed, damage);
    // TODO Auto-generated constructor stub
  }

}
