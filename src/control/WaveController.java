package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.StatusOfArmiesView;

public class WaveController implements ActionListener {
  
  private int i;
  
  private StatusOfArmiesView statusOfArmiesView;

  /**
   * Constructor
   * @param statusOfArmiesView
   * @param i
   */
  public WaveController(StatusOfArmiesView statusOfArmiesView, int i) {
    
    this.i = i;
    
    this.statusOfArmiesView = statusOfArmiesView;
    
  }
  
  @SuppressWarnings("deprecation")
  @Override
  /**
   * Action of btn WAVE
   * stop send army thread
   * OR stop waiting thread
   * start Wave thread
   */
  public void actionPerformed(ActionEvent e) {
    
    statusOfArmiesView.sendArmyTimeThreads.get(i).stop();
//    statusOfArmiesView.timeThreads.remove(i);
    
    statusOfArmiesView.sendArmyThreads.get(i).stop();
//    statusOfArmiesView.sendArmyThreads.remove(i);
    
    TimeThread timeThread = new TimeThread(statusOfArmiesView.labels.get(i), 10000, "Time come back");
    
    WaveThread waveThread = new WaveThread(statusOfArmiesView, i);
    
    statusOfArmiesView.btns.get(i).setVisible(false);
    
    timeThread.start();
    waveThread.start();
  }

}
