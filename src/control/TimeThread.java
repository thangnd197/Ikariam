package control;

import javax.swing.JLabel;

public class TimeThread extends Thread {
  
  
  private JLabel label;
  
  private int time;
  
  private int temp;
  
  private String text;
  
  /**
   * Constructor
   * @param label
   * @param time
   * @param text
   */
  public TimeThread(JLabel label, int time, String text) {

    this.label = label;
    
    this.time = time;
    
    this.text = text;
  
  }
  
  @Override
  /**
   * show time of thread to statusOfView
   */
  public void run() {
    
    for (int i = 0; i < time; i++) {
      
      temp = time - i - 1;
      
      label.setText(text + ": " + temp + "/" + time + "(s)" );
      
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        
        e.printStackTrace();
      }

    }
    
    if (text.equals("Time arriving")) {
      label.setText("waiting");
    }
    
    super.run();
  }
  
}
