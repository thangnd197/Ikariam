package control;

import model.army.Army;
import model.island.Island;
import view.AttackArmySettingView;

public class SendArmyThread extends Thread {
  
  private int indexOfTown;
  
  private Island island;
  
  private AttackArmySettingView attackArmySettingView;
  
  private Army army;
  
  public ProcessBattleThead thread;
  
  private int amountOfArmy;
  
  /**
   * Constructor
   * 
   * @param indexOfTown
   * @param island
   * @param attackArmySettingView
   * @param army
   * @param amountOfArmy
   */
  public SendArmyThread(int indexOfTown, Island island, 
      AttackArmySettingView attackArmySettingView, Army army, int amountOfArmy) {
    
    super();
    this.indexOfTown = indexOfTown;
    this.island = island;
    this.attackArmySettingView = attackArmySettingView;
    this.army = army;
    this.amountOfArmy = amountOfArmy;
    
  }



  @Override
  public void run() {
    
    // Delay 10 s
    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      
      e.printStackTrace();
    }
    
    // check battle is running
    if (attackArmySettingView.getReserve().getArmies().isEmpty()) {
      
      attackArmySettingView.getReserve().getArmies().add(army);
      
      thread = 
      new ProcessBattleThead(attackArmySettingView.getReserve(), indexOfTown, island,
                            attackArmySettingView.statusOfArmiesView, amountOfArmy);
      
      thread.start();
      
    } else {
      attackArmySettingView.getReserve().getArmies().add(army);
    }
  }
}
