package control;

import view.StatusOfArmiesView;

public class WaveThread extends Thread {
  
  public StatusOfArmiesView statusOfArmiesView;
  
  public int i;
 
  /**
   * Constructor
   * @param statusOfArmiesView
   * @param i
   */
  public WaveThread(StatusOfArmiesView statusOfArmiesView, int i) {
    
    this.statusOfArmiesView = statusOfArmiesView;
    
    this.i = i;
    
  }
  
  
  @Override
  /**
   * delay 10s, after that , start action come home
   */
  public void run() {
    
    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    statusOfArmiesView.labels.get(i).setText("came home");
    super.run();
  }
}
