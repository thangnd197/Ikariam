package control;

import model.army.Army;
import model.battleField.BattleField;
import model.battleField.Reserve;
import model.island.Island;
import view.BattleView;
import view.StatusOfArmiesView;

public class ProcessBattleThead extends Thread {
  
  private Reserve reserve;
  
  private int indexOfTown;
  
  private Island island;
  
  private StatusOfArmiesView statusOfArmiesView;
  
  private int amountOfArmy;
  
  /**
   * Constructor
   * 
   * @param reserve
   * @param indexOfTown
   * @param island
   * @param statusOfArmiesView
   * @param amountOfArmy
   */
  public ProcessBattleThead(Reserve reserve, int indexOfTown, Island island, 
      StatusOfArmiesView statusOfArmiesView, int amountOfArmy) {

    this.reserve = reserve;
    
    this.indexOfTown = indexOfTown;
    
    this.island = island;
    
    this.amountOfArmy = amountOfArmy;
    
    this.statusOfArmiesView = statusOfArmiesView;
    
  }
  
  @Override
  public void run() {
    
    /**
     * Loop with number of round of battle
     */
    for (int i = 0; i < 10; i++) {
      
      // Create army
      Army army = reserve.getAllArmy();
      
      Army enermy = island.getTowns()[indexOfTown].getArmy().getLiveArmy();
      
      // Create battle field
      BattleField myBattle = new BattleField(island.getTowns()[indexOfTown].getLevel(), army);
      
      BattleField enermyBattle = new BattleField(island.getTowns()[indexOfTown].getLevel(), enermy);
      
      // Process attack
      
      enermyBattle.processRound(myBattle);
      
      myBattle.processRound(enermyBattle);
      
      // Truyen battle cho battle view
      try {
        
        new BattleView(myBattle, enermyBattle, island.getTowns()[indexOfTown].getLevel());

      } catch (Exception e1) {
        
        e1.printStackTrace();
        
      }
      
      // Show waiting time on statusOfArmies
      TimeThread waittingTimeThread = new TimeThread(statusOfArmiesView.labels.get(amountOfArmy), 2, "Waiting time");
      
//      statusOfArmiesView.waitingTimeThreads.add(waittingTimeThread);
      
      waittingTimeThread.start();
      
      
      // Delay 2s
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        
        e.printStackTrace();
      }
      

    }
    
    super.run();
  }
}
